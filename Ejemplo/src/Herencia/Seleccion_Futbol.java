/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Herencia;

/**
 *
 * @author TRABAJOS
 */
public class Seleccion_Futbol {
    int id;
    String nombre;
    String apellidos;
    int edad;

    public Seleccion_Futbol(int id, String nombre, String apellidos, int edad) {
        this.id = id;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.edad = edad;
    }

    public String getAtributos() {
        return "Id:  " + id
                + "\nNombre: " + nombre
                + "\nApellidos:" + apellidos
                + "\nEdad: " + edad;
    }
    
    public void concentracion(){
        System.out.println("Concentrado ");
    }
    
    public void viajar(){
        System.out.println("Viajando");
    }
}
