/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Herencia.ejercicios;

/**
 *
 * @author TRABAJOS
 */
public class Congelado extends  Producto{
    private int temperatura;

    public Congelado(int temperatura, String fechaCaducidad, int numLote, String pais, String fechaEnvasdado) {
        super(fechaCaducidad, numLote, pais, fechaEnvasdado);
        this.temperatura = temperatura;
    }
    
    public String getAtributos() {
        return "Temperatura: " + temperatura
                + "\n0Fecha Caducidad: " + fechaCaducidad
                + "\nNumero de lote: " + numLote
                + "\nPais de origen:" +pais
                + "\nFceha de envasado: " + fechaEnvasdado;
    }
   
   public void tipoAire(){
       System.out.println("Congelado tipo Aire");
       System.out.println("20% Nitrogeno");
       System.out.println("10% Oxigeno");
       System.out.println("20% Dioxido de Carbono");
       System.out.println("50& Vapor de aire");
}
   
   public void tipoAgua(){
       System.out.println("Congelado tipo Agua");
       int sal = 10;
       int agua = 50;
       int salinidad = sal * agua;
       System.out.println("Salinidad del agua: " + salinidad);
   }
   
   public void tipoNitrogeno(){
          System.out.println("Congelado tipo Nitrogeno");
       System.out.println("Metodo  Utilizado: Criogenico");
       System.out.println("10000 segundos  utilizados");
   }
   
}





