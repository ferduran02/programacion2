/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Herencia;

/**
 *
 * @author TRABAJOS
 */
public class Masajita extends Seleccion_Futbol{
    private String titulacion;
    private int aniosExperiencia;

    public Masajita(String titulacion, int aniosExperiencia, int id, String nombre, String apellidos, int edad) {
        super(id, nombre, apellidos, edad);
        this.titulacion = titulacion;
        this.aniosExperiencia = aniosExperiencia;
    }
    
    public String getAtributos ( ){
         return "Id: " + id
                + "\nNombre: " + nombre
                + "\nApellidos:" +apellidos
                + "\nEdad: " + edad
                + "\nTitulacion: " + titulacion
                + "\nAños de experiencia: " + aniosExperiencia;
    }
    
    public void concentracion(){
        System.out.println("Si ");
    }
    
    public void viajar(){
        System.out.println("Viajando");
    }
    
    public void darMasajes(){
        System.out.println("Si");
    }
}

