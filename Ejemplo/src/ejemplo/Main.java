/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo;

import javax.swing.JOptionPane;
import javax.swing.plaf.OptionPaneUI;

/**
 *
 * @author estudiante
 */
public class Main {

    public static void main(String[] args) {

        Metodo log = new Metodo();

        String menu = "Ejercicios\n"
                + "1. Numero de la suerte\n"
                + "2. Numero perfecto\n"
                + "3. Numero random\n"
                + "4. Personas\n"
                + "5. Riman\n"
                + "6. Salir";

        int op = Integer.parseInt(JOptionPane.showInputDialog(menu));

        switch (op) {
            case 1:
                int dia = Integer.parseInt(JOptionPane.showInputDialog("Dia de nacimiento: "));
                int mes = Integer.parseInt(JOptionPane.showInputDialog("Mes de nacimiento: "));
                int anno = Integer.parseInt(JOptionPane.showInputDialog("Año de nacimiento: "));
                JOptionPane.showMessageDialog(null, "Su numero de la suerte es: " + log.numSuerte(dia, mes, anno));
                break;
            case 2:
                int n = Integer.parseInt(JOptionPane.showInputDialog("DigIte un numero: "));
                JOptionPane.showMessageDialog(null, log.perfecto(n));

            case 3:
                int random = log.random();
                for (int i = 0; i < 5; i++) {
                    int num = Integer.parseInt(JOptionPane.showInputDialog("Digite un numero: "));
                    if (log.decision(num, random)) {
                        JOptionPane.showMessageDialog(null, " HAZ GANADO");
                        break;
                    } else if (i == 5) {
                        JOptionPane.showMessageDialog(null, "SE TE ACABARON LOS TURNOS");
                        break;
                    }
                    JOptionPane.showMessageDialog(null, log.pista(num, random));
                    JOptionPane.showMessageDialog(null, "Te quedan " + (4 - i) + " intentos! SUERTE");
                }
                break;
            case 4:
                String[][] matriz = new String[20][2];
                for (int i = 0; i < matriz.length; i++) {
                    for (int j = 0; j < matriz[i].length; j++) {
                        String nombre = JOptionPane.showInputDialog("Digite su nombre: ");
                        int edad = Integer.parseInt(JOptionPane.showInputDialog("Digite su edad"));
                        if (matriz[i][j] == null) {
                            matriz[i][j] = nombre + " " + String.valueOf(edad);
                            if (edad % 2 == 0) {
                                System.out.println(matriz[i][j] + "\033[34m");
                            } else {
                                System.out.println(matriz[i][j] + "\033[31m");
                            }
                        }

                    }

                }
            case 5:
                String palabra_uno = JOptionPane.showInputDialog("Digite la primer palabra: ");
                String palabra_dos = JOptionPane.showInputDialog("Digite la segunda palabra: ");
                JOptionPane.showMessageDialog(null, log.riman(palabra_uno, palabra_dos));

        }

    }
}
