/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Herencia;

/**
 *
 * @author TRABAJOS
 */
public class Futbolista extends Seleccion_Futbol{
    
    private int dorsal;
    private String demarcacion;

    public Futbolista(int id, String nombre, String apellidos, int edad, int dorsal, String demarcacion) {
        super(id, nombre, apellidos, edad);
        this.dorsal = dorsal;
        this.demarcacion = demarcacion;
    }
    
   public String getAtributos() {
        return "Id: " + id
                + "\nNombre: " + nombre
                + "\nApellidos:" +apellidos
                + "\nEdad: " + edad
                + "\nDorsal: " + dorsal
                + "\nDemarcacion: " + demarcacion;
    }
    public void concentracion(){
        System.out.println("Concentramiento");
    }
    
    public void viajar(){
        System.out.println("Viajando");
    }
    
    public void jugar_partido(){
        System.out.println("Jugando Martes");
    }
    
    public void entrenar(){
        System.out.println("Entrenando");
    }
    
    
    
}
