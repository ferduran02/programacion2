/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Herencia;

/**
 *
 * @author TRABAJOS
 */
public class Entrenador extends Seleccion_Futbol{
    public String idFedereacion;

    public Entrenador(String idFedereacion, int id, String nombre, String apellidos, int edad) {
        super(id, nombre, apellidos, edad);
        this.idFedereacion = idFedereacion;
    }
    
    
    public String getAtributos(){
                      return "Id:  " + id
                + "\nNombre: " + nombre
                + "\nApellidos:" + apellidos
                + "\nEdad: " + edad
                + "\nIdFederacion: " + idFedereacion;

    }
    
    public void concentracion(){
        System.out.println("Si ");
    }
    
    public void viajar(){
        System.out.println("Viajando");
    }
    
    public void dirigirpartido(){
        System.out.println("Si");
    }
    
    public void dirigirentrenamiento(){
        System.out.println("No");
    }
}
