/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo;

import javax.swing.JOptionPane;

/**
 *
 * @author TRABAJOS
 */
public class Metodo {
    
    public int numSuerte(int dia, int mes, int anno) {
        int suma = dia + mes + anno;

        int n1 = suma / 1000;
        suma -= n1 * 1000;
        int n2 = suma / 100;
        suma -= n2 * 100;
        int n3 = suma / 10;
        suma -= n3 * 10;
        int n4 = suma;

        int suerte = n1 + n2 + n3 + n4;

        return suerte;

    }

    public String perfecto(int n) {
        int sum = 0;
        for (int i = 1; i < n; i++) {
            if (n % i == 0) {
                sum = sum + i;
            }
        }
        if (sum == n) {
            return "Su numero es perfecto";
        } else {
            return "Su numero no es perfecto";
        }
    }

    public int random() {
        int min = 1;
        int max = 7;

        int devolver = (int) (Math.random() * max) + min;
        return devolver;
    }

    public boolean decision(int num, int random) {
        return num == random;
    }

    public String pista(int num, int random) {
        if (num > random) {
            return "Digite un numero menor";
        } else {
            return "Digite un numero mayor";
        }
    }

    public String riman(String palabra_uno, String palabra_dos) {
        if (palabra_uno.substring(palabra_uno.length() - 3, palabra_uno.length()).equals(palabra_dos.substring(palabra_dos.length() - 3, palabra_dos.length()))) {
            return "RIMAN";
        } else if (palabra_uno.substring(palabra_uno.length() - 2, palabra_uno.length()).equals(palabra_dos.substring(palabra_dos.length() - 2, palabra_dos.length()))) {
            return "RIMAN POCO";
        } else {
            return "NO RIMAN";
        }
    }

}
